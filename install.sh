#!/bin/sh

# Setup vim

if type git &>/dev/null
then
    # install Vundle
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

    # if we have vim, run PluginInstall to get all plugins managed by vundle
    if type vim &>/dev/null
    then
        vim +PluginInstall +qall
    else
        echo "Vim is not installed"
        exit 1
    fi
    # Fix ~/.vimrc
    if [ -f ~/.vimrc ]
    then
        mv ~/.vimrc ~/.vimrc.backup.mvanbaak
    fi
    echo "source ~/.vim/vimrc" > ~/.vimrc
else
    echo "Git is not installed"
    exit 1
fi
